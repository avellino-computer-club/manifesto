# MANIFESTO

## Art. 0
È costituito l'**Avellino Computer Club**.

##  Art. 1
Nella convinzione che l’informatica e la telematica siano parte essenziale della cultura del mondo moderno, l'**Avellino Computer Club** si propone di favorire la più ampia diffusione della conoscenza in questi campi rendendosi promotore di eventi ed attività sul territorio e nel cyberspazio.

## Art. 2
L'**Avellino Computer Club** crede nella libera circolazione della conoscenza e quindi, operando in ambito informatico, sostiene la filosofia del software libero e dell’Open Source.

## Art. 3
L'**Avellino Computer Club** si impegna in particolare a promuovere l’uso e la diffusione di variegate tecnologie informatiche, prediligendo, ove reso possibile dagli interessi dei membri e/o dalla disponibilità di risorse, l'uso di software open-source.

## Art. 4
L'**Avellino Computer Club**, per raggiungere i propri scopi, si propone attività quali:

* organizzazione di incontri pubblici e corsi;
* distribuzione di software libero e documentazione (compatibilmente con le leggi sul diritto d’autore vigenti nel nostro paese);
* sviluppo di software libero e produzione o traduzione di documenti relativi ad esso, anch’essi liberamente distribuibili;
* attività di consulenza nei confronti dei soci;
* utilizzo degli strumenti telematici e di comunicazione per la promozione dei propri scopi;
* ogni altra attività che sarà ritenuta opportuna.

## Art. 5
L’attività dell'**Avellino Computer Club** si rivolge principalmente alla provincia di Avellino. Tuttavia, coerentemente con le proprie finalità, 
l'**Avellino Computer Club** può partecipare a progetti più ampi in collaborazione con altre associazioni analoghe.

